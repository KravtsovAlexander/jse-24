package ru.t1.kravtsov.tm.command.task;

import org.jetbrains.annotations.NotNull;

public final class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Remove all tasks.";

    @NotNull
    public static final String NAME = "task-clear";

    @Override
    public void execute() {
        System.out.println("[TASK CLEAR]");
        getTaskService().deleteAll(getUserId());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
