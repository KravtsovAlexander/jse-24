package ru.t1.kravtsov.tm;

import ru.t1.kravtsov.tm.api.component.IBootstrap;
import ru.t1.kravtsov.tm.component.Bootstrap;

public final class Application {

    public static void main(String[] args) {
        final IBootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}